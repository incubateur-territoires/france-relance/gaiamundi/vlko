
### dispositif de cartographie interactive de la ville de lyon  


****Le site est actuellement en cours d'analyse pour une refonte prévue au premier trimestre 2022 visant à faciliter la navigation et l'accès aux cartes et données  
Dans l'attente, nous allégeons le site en supprimant les productions antérieures à 2019. L'ancien site reste accessible [ICI.](http://vlko.org/archives/site0/index0.html#art_C1-Accueil)  
Des perturbations occasionnelles sont possibles.****


****VLKO rassemble les productions de cartographie interactive de la Ville de Lyon et les démarches coopératives et collaboratives associées :****

Problématiser, connaître son territoire, s'outiller pour débattre, ... Construit à partir des besoins et usages, cet ensemble constitue une importante ressource accessible à tous ainsi qu'une contribution à l'OpenData.  
VLKO, c'est aussi de nombreuses cartes commentées dans des articles que vous pouvez consulter via le menu déroulant à droite.  
Le plus difficile demeure de savoir précisément les questions que l'on se pose et les matériaux dont on dispose pour en discuter !  
En premier lieu, consultez toujours la [communauté des scenarios](http://vlko.org/sc1/touslessecnarios.html) et son **moteur de recherche** : c'est là que vous trouverez le plus aisément les noms des PageCarto et les types de données que vous pouvez mobiliser  
**Nous questionner :** [ddt.gaiamundi@mairie-lyon.fr](mailto:ddt.gaiamundi@mairie-lyon.fr)


****Quelques exemples d'usages :****

* Cibler des populations clés des politiques municipales à Lyon : identifier où les femmes se font le moins dépister pour le cancer du sein afin d'y mettre en place des actions de prévention adaptées
* Identifier les besoins de la population en proximité d'un centre social : la population est-elle jeune, a-t-elle des difficultés particulières ou au contraire des besoins exprimés ?
* Pour une épicerie sociale, quel est le public le plus en difficulté après duquel cibler les accompagnements ?


**De quoi parlons-nous ?**



* De modules cartographiques ["PageCarto"](http://www.vlko.org/site0/index0.html#art_C2-Voir@@Inserer@@et@@Telecharger@@les@@PageCarto) interactifs et téléchargeables.  

    Voir l'exemple du [module sur les quartiers anciens](http://vlko.org/sc1/indexVoirCarte.html?carte=%20http://vlko.org/sc1/SuiteCairo-DevTerritorial2/Lyon_Quartiers_Anciens_2018/PageCartoDossier/#cadre) dont la carte ci-dessous est extraite (survolez les quartiers pour visualiser les données par quartier)  




* De modules permettant de réaliser par soi-même des [**scénarios**](http://www.vlko.org/site0/index0.html#art_C2-Acceder@@a@@lespace@@Cartes@@Donnees@@et@@Scenarios) en combinant les données et les cartes des différents modules PageCarto existants, comma dans l'exemple ci-dessous  réalisé comme support de discussion pour un groupe de travail sur la santé des lyonnais.  




**Carte : support préparatoire pour la Commission Santé du 9ème arrondissement - juillet 2018**  
